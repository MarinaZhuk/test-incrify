const gulp = require("gulp");
const sass = require("gulp-sass");
const cssnano = require("gulp-cssnano");
const autoprefixer = require('gulp-autoprefixer');
const imagemin = require('gulp-imagemin');
const concat = require("gulp-concat");


gulp.task('html', function() {
    return gulp.src("src/*.html")
    .pipe(gulp.dest("dist"));
});

gulp.task('sass', function () {
  return gulp.src('src/sass/**/*.scss')
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(gulp.dest("dist"));
});

gulp.task('imgs', function() {
    return gulp.src("src/img/*.+(jpg|jpeg|png|gif)")
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{ removeViewBox: false }],
            interlaced: true
        }))
        .pipe(gulp.dest("dist"));
});

gulp.task('watch', function() {
    gulp.watch('src/*.html', gulp.series["html"]);
    gulp.watch('src/sass/**/*.scss', gulp.series["sass"]);
    gulp.watch('src/img/*.+(jpg|jpeg|png|gif)', gulp.series["imgs"]);
});


gulp.task("default", gulp.series("html", "sass", "imgs", "watch"));
